//
//  Recipe.swift
//  Diced Onions
//
//  Created by Tran Kim Thach on 5/5/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import Foundation

class Recipe {
    var name: String?
    var id: String?
    var ingredients: [String] = []
    var instructions: [String] = []
    var comments: [String] = []
    var tags: [String] = []
    
    init(id: String?, name: String?, ingredients: [String], instructions: [String], comments: [String], tags: [String]) {
        self.id = id
        self.name = name
        self.ingredients = ingredients
        self.instructions = instructions
        self.comments = comments
        self.tags = tags
    }
}


