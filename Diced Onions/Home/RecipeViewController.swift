//
//  RecipeViewController.swift
//  Diced Onions
//
//  Created by Tran Kim Thach on 4/5/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class RecipeViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate {
    
    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var fab: UIButton!
    lazy var db = Firestore.firestore()
    var recipes: CollectionReference?
    var newTags: [String] = []
    var authController: Auth?
    

    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var slides: [Details] = []
    var recipe: Recipe?

    
    @IBOutlet weak var rightBarButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        authController = Auth.auth()
        
        authController?.signInAnonymously() { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
        }
        
        recipes = db.collection("recipes")
        
        slides = setDetails()
        populateWithRecipeDetails()
        setupSlideScrollView(slides: slides)

        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
        
        // If there's no recipe id -> user is creating a new recipe
        if recipe!.id == nil {
            fab.isHidden = true
        }

    }
    
    // Pass this recipe to the 2nd tab
    @IBAction func fabTapped(_ sender: Any) {
        tabBarController?.selectedIndex = 1
        let navController = tabBarController?.selectedViewController as! UINavigationController
        let targetVC = navController.viewControllers.first as! CookingTableViewController
        targetVC.allRecipes.append(recipe!)
        // Quite inefficient
        targetVC.tableView.reloadData()
        // Return to Home
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
        
    }
    
    @IBAction func rightBarButtonTapped(_ sender: Any) {
        upload()
        
    }
    
    func upload() {
        var ingredients: [String] = []
        var instructions: [String] = []
        var comments: [String] = []
        
        let ingrTextViews = slides[0].vStackView.arrangedSubviews
        for view in ingrTextViews {
            if view is UITextView {
                let text = (view as! UITextView).text!
                if text != "" {
                    ingredients.append(text)
                }
            }
        }
        
        let insTextViews = slides[1].vStackView.arrangedSubviews
        for view in insTextViews {
            if view is UITextView {
                let text = (view as! UITextView).text!
                if text != "" {
                    instructions.append(text)
                }
            }
        }
        
        let cmtTextViews = slides[2].vStackView.arrangedSubviews
        for view in cmtTextViews {
            if view is UITextView {
                let text = (view as! UITextView).text!
                if text != "" {
                    comments.append(text)
                }
            }
        }
        
        
        if (recipe?.id == nil) {
            recipes?.addDocument(data: [
                "name": titleTextField.text!,
                "ingredients": ingredients,
                "instructions": instructions,
                "comments": comments,
                "tags": newTags
            ]) { err in
                if let err = err {
                    print("Error adding recipe: \(err)")
                } else {
                    print("Recipe successfully added")
                }
            }
        }
        else {
            recipes?.document(recipe!.id!).updateData([
                "name": titleTextField.text!,
                "ingredients": ingredients,
                "instructions": instructions,
                "comments": comments,
                "tags": newTags
            ]) { err in
                if let err = err {
                    print("Error updating recipe: \(err)")
                } else {
                    print("Recipe successfully updated")
                }
            }
        }
        
        
        // Return to Home
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Initiate 3 slides for ingredients, instructions and comments
    func setDetails() -> [Details] {
        let slide1: Details = Bundle.main.loadNibNamed("Details", owner: self, options: nil)?.first as! Details
        slide1.addButton.layer.cornerRadius = 10
        slide1.addButton.addTarget(self, action: #selector(addButtonTapped(_ :)), for: .touchUpInside)
        slide1.subTitle.attributedText = NSAttributedString(string: "Ingredients", attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        let slide2: Details = Bundle.main.loadNibNamed("Details", owner: self, options: nil)?.first as! Details
        slide2.addButton.layer.cornerRadius = 10
        slide2.addButton.addTarget(self, action: #selector(addButtonTapped(_ :)), for: .touchUpInside)

        slide2.subTitle.attributedText = NSAttributedString(string: "Instructions", attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])

        
        let slide3: Details = Bundle.main.loadNibNamed("Details", owner: self, options: nil)?.first as! Details
        slide3.addButton.layer.cornerRadius = 10
        slide3.addButton.addTarget(self, action: #selector(addButtonTapped(_ :)), for: .touchUpInside)
        slide3.subTitle.attributedText = NSAttributedString(string: "Comments", attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])

        
        return [slide1, slide2, slide3]
    }
    
    func textViewForThis(item: String, index: Int) -> UITextView {

        let newTextView = UITextView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0))
        let vStackView = slides[index].vStackView!
        // Alternating between 2 colors
        if slides[index].preferredBackgroundColorIsAzul {
            newTextView.backgroundColor = Details.COLOR_AZUL
        }
        else {
            newTextView.backgroundColor = Details.COLOR_MALBEC
        }
        slides[index].preferredBackgroundColorIsAzul.toggle()

        newTextView.text = item
        newTextView.textColor = .white
        newTextView.font = .systemFont(ofSize: 16, weight: .semibold)
        newTextView.layer.cornerRadius = 10
        newTextView.adjustsFontForContentSizeCategory = true
        newTextView.contentMode = .scaleToFill
        newTextView.isEditable = false
        newTextView.translatesAutoresizingMaskIntoConstraints = false
        // Add the new textview to the bottom of the stack, ahead of the button
        vStackView.insertArrangedSubview(newTextView, at: vStackView.arrangedSubviews.count - 1)
        
        newTextView.translatesAutoresizingMaskIntoConstraints = true
        newTextView.isScrollEnabled = false
        newTextView.sizeToFit()
        newTextView.translatesAutoresizingMaskIntoConstraints = false
        
        // When tapped, the textview will allow/ disallow editing
        let longPressGesture = UILongPressGestureRecognizer(target: self, action:  #selector (self.toggleTextViewEditing (_:)))
        newTextView.addGestureRecognizer(longPressGesture)
        
        return newTextView
        
    }
    
    func populateWithRecipeDetails() {
        
        if recipe!.name != nil {
            titleTextField.text = recipe!.name!
        }

        let ingredients = recipe!.ingredients
        let instructions = recipe!.instructions
        let comments = recipe!.comments
        
        for i in 0..<ingredients.count {
            let _ = textViewForThis(item: ingredients[i], index: 0)
        }

        for i in 0..<instructions.count {
            let _ = textViewForThis(item: instructions[i], index: 1)
        }
        
        for i in 0..<comments.count {
            let _ = textViewForThis(item: comments[i], index: 2)
        }
        
    }
    
    func setupSlideScrollView(slides: [Details]) {
        // If I do this and disable vertical scrolling, when we add more items
        // the view can't scroll down to them
//        scrollView.contentSize.width = view.frame.width * CGFloat(slides.count)
//        scrollView.isPagingEnabled = true
//        for i in 0 ..< slides.count {
//            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: scrollView.frame.height)
//            scrollView.addSubview(slides[i])
//
//        }
        
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
    }
    

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // For re-orientation. Because the frame of UIScrollView is recreated, we need to recall this function
        setupSlideScrollView(slides: slides)
    }
    
    
    @objc func toggleTextViewEditing(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .ended {
            let textView = sender.view as! UITextView
            textView.isEditable.toggle()
            if textView.isEditable {
                textView.becomeFirstResponder()
            }
            else {
                textView.resignFirstResponder()
            }
        }

    }
    
    @objc func addButtonTapped(_ sender: UIButton) {
        
        /* Look for the title, then proceed to insert
                                new entry on the correct slide*/
        let title = (sender.superview?.superview as! Details).subTitle.text
        var newTV: UITextView?
        if (title == "Ingredients") {
            newTV = textViewForThis(item: "New Ingredient", index: 0)
        }
        else if (title == "Instructions") {
            newTV = textViewForThis(item: "New Instruction", index: 1)
        }
        else if (title == "Comments") {
            newTV = textViewForThis(item: "New Comment", index: 2)
        }
        
        // Make the new textview on focus
        newTV!.isEditable = true
        newTV!.becomeFirstResponder()
        
    }
    
    
     // MARK: - Navigation
     
     // Send the list of selected tags to the TagVC
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTagSegue" {
            let destination = segue.destination as! TagTableViewController
            
            let selected = recipe!.tags.reduce(into: [String: Bool]()) { $0[$1] = true }

            destination.selected = selected
        }
        
     }
    
    // Get the selected tags from the tag VC
    @IBAction func unwindToDetails(segue: UIStoryboardSegue) {
        if let sourceViewController = segue.source as? TagTableViewController {
            /* Can't save directly to recipe.tags because for some reason
             doing that would overwrite the instance in HomeTableVC
             And I need the old tag list to remove them from Firestore tag tree
             */
            newTags = Array(sourceViewController.selected.keys)
        }
    }


}
