//
//  TagTableViewCell.swift
//  Diced Onions
//
//  Created by Tran Kim Thach on 26/5/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit

class TagTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            accessoryType = .checkmark
        } else {
            accessoryType = .none
        }
    }

}
