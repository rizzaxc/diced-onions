//
//  CookingTableViewController.swift
//  Diced Onions
//
//  Created by Tran Kim Thach on 27/5/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit
import SwiftEntryKit

class CookingTableViewController: UITableViewController, UISearchResultsUpdating {
    
    let searchController = UISearchController(searchResultsController: nil)
    var allRecipes: [Recipe] = []
    var filteredRecipes: [Recipe] = []


    override func viewDidLoad() {
        super.viewDidLoad()
        // Configure searchController
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Recipes"
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    // - MARK: Search functionality
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        // Returns true if the searchController is active & searchBar not empty
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        // All tasks with titles matching the search text is returned to filteredTask
        filteredRecipes = allRecipes.filter({( recipe : Recipe) -> Bool in
            let nameMatched = recipe.name?.lowercased().contains(searchText.lowercased())
            return nameMatched!
        })
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }

    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // If filtering, return the number of items in filterdTask
        if isFiltering() {
            return filteredRecipes.count
        }
        
        // Otherwise, return count of all tasks
        return allRecipes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipeCell", for: indexPath)
        let recipe: Recipe
        if isFiltering() {
            recipe = filteredRecipes[indexPath.row]
        }
        else {
            recipe = allRecipes[indexPath.row]
        }
        cell.textLabel?.text = recipe.name
        return cell
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        // Remove a recipe from the list
        if editingStyle == .delete {
            self.allRecipes.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.toastAfterDelete(message: "Recipe successfully removed")

        }
        
    }
    
    func deleteAfterFinish(_ indexPath: IndexPath) {
        // Delete the row then display a congrat toast
        self.allRecipes.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        
        self.toastAfterDelete(message: "Congratulations on finishing the dish")
        
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cookingDetailsSegue" {
            if let row = tableView.indexPathForSelectedRow?.row {
                let recipe: Recipe
                if isFiltering() {
                    recipe = filteredRecipes[row]
                }
                else {
                    recipe = allRecipes[row]
                }
                let destination = segue.destination as! CookingDetailViewController
                destination.recipe = recipe
                destination.recipeIndexPath = tableView.indexPathForSelectedRow
                
            }
            
        }
    }
    

}
