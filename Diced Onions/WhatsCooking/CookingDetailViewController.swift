//
//  CookingDetailViewController.swift
//  Diced Onions
//
//  Created by Tran Kim Thach on 8/6/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit
import UserNotifications
import SwiftEntryKit

class CookingDetailViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var titleTextField: UITextField!

    var recipeIndexPath: IndexPath? /* This is used to delete the row after finish */
    let TIMER = "timer"
    var slides: [Details] = []
    var recipe: Recipe?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Request permission to use local notifications
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert])
        { (granted, error) in
            if !granted {
                print("Permission was not granted!")
                return
            }
        }
        
        slides = setDetails()
        populateWithRecipeDetails()
        setupSlideScrollView(slides: slides)

        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
        
    }
    
    @IBAction func finishButtonTapped(_ sender: Any) {
        tabBarController?.selectedIndex = 1
        let navController = tabBarController?.selectedViewController as! UINavigationController
        let targetVC = navController.viewControllers.first as! CookingTableViewController
        targetVC.deleteAfterFinish(recipeIndexPath!)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetButtonTapped(_ sender: Any) {
        /* Reset all entries to normal */
        for i in 0..<2 {
            let entries = slides[i].vStackView.arrangedSubviews
            for entry in entries {
                if entry is UITextView {
                    let textView = entry as! UITextView
                    textView.attributedText = NSAttributedString(string: textView.text) // Drop all attributes
                    textView.font = .systemFont(ofSize: 16, weight: .semibold)
                    textView.textColor = .white
                }
            }
        }
    }
    
    
    func setDetails() -> [Details] {
        let slide1: Details = Bundle.main.loadNibNamed("Details", owner: self, options: nil)?.first as! Details
        slide1.addButton.isHidden = true
        slide1.subTitle.attributedText = NSAttributedString(string: "Ingredients", attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        let slide2: Details = Bundle.main.loadNibNamed("Details", owner: self, options: nil)?.first as! Details
        slide2.addButton.isHidden = true
        slide2.subTitle.attributedText = NSAttributedString(string: "Instructions", attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        
        return [slide1, slide2]
    }
    
    func populateWithRecipeDetails() {
        
        if recipe!.name != nil {
            titleTextField.text = recipe!.name!
        }
        
        let ingredients = recipe!.ingredients
        let instructions = recipe!.instructions
        for i in 0..<ingredients.count {
            textViewForThis(item: ingredients[i], index: 0)
        }
        
        for i in 0..<instructions.count {
            textViewForThis(item: instructions[i], index: 1)
        }
        
    }
    
    func textViewForThis(item: String, index: Int) {
        
        let newTextView = UITextView()
        let vStackView = slides[index].vStackView!
        if slides[index].preferredBackgroundColorIsAzul {
            newTextView.backgroundColor = Details.COLOR_AZUL
        }
        else {
            newTextView.backgroundColor = Details.COLOR_MALBEC
        }
        slides[index].preferredBackgroundColorIsAzul.toggle()
        
        newTextView.text = item
        newTextView.textColor = .white
        newTextView.font = .systemFont(ofSize: 16, weight: .semibold)
        newTextView.layer.cornerRadius = 10
        newTextView.adjustsFontForContentSizeCategory = true
        newTextView.contentMode = .scaleToFill
        newTextView.isEditable = false
        newTextView.isSelectable = false
        newTextView.translatesAutoresizingMaskIntoConstraints = false
        // Add the new textview to the bottom of the stack, ahead of the button
        vStackView.insertArrangedSubview(newTextView, at: vStackView.arrangedSubviews.count - 1)
        
        newTextView.translatesAutoresizingMaskIntoConstraints = true
        newTextView.isScrollEnabled = false
        newTextView.sizeToFit()
        newTextView.translatesAutoresizingMaskIntoConstraints = false
        
        
        // When tapped, the text will toggle between strikethrough/ normal
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.strikeThroughToggle (_:)))
        newTextView.addGestureRecognizer(gesture)
        
        // When long pressed, each instruction can associate a timer with it
        if (index == 1) {
            let gesture = UILongPressGestureRecognizer(target: self, action: #selector(self.addTimer (_:)))
            newTextView.addGestureRecognizer(gesture)
        }

    }

    
    
    func setupSlideScrollView(slides: [Details]) {
        scrollView.contentSize.width = view.frame.width * CGFloat(slides.count)
        scrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: scrollView.frame.height)
            scrollView.addSubview(slides[i])
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // For re-orientation. Because the frame of UIScrollView is recreated, we need to recall this function
        setupSlideScrollView(slides: slides)
    }
    
    @objc func strikeThroughToggle(_ sender: UITapGestureRecognizer) {
        let view = sender.view as! UITextView
        var range = NSRange(location: 0, length: view.attributedText!.length)
        
        // If text has no strikethrough, make it strikethrough and vice versa
        if view.attributedText.attribute(NSAttributedString.Key.strikethroughStyle, at: 0, effectiveRange: &range) == nil {
            view.attributedText = NSAttributedString(string: view.text, attributes:
                [.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            
        }
        else {
            view.attributedText = NSAttributedString(string: view.text) // Drop all attributes
            view.font = .systemFont(ofSize: 16, weight: .semibold)
        }
        view.textColor = .white

        
    }
    
    @objc func addTimer(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .ended {
            /*
            Create label, textview, 2 buttons and a timer
            Position them using stack views
            Then display them over the current screen
            Using SwiftEntryKit
             */
            let title = UILabel()
            title.isHighlighted = true
            title.isUserInteractionEnabled = false
            title.text = "Set a Timer"
            title.textAlignment = .center
            title.font = UIFont.systemFont(ofSize: 22.0, weight: .medium)

            
            let desc = UITextView()
            desc.text = "You can attach a timer to this instruction. When time is up, you will be notified."
            desc.font = UIFont.systemFont(ofSize: 17.0)
            desc.isEditable = false
            desc.translatesAutoresizingMaskIntoConstraints = true
            desc.isScrollEnabled = false
            desc.sizeToFit()
            desc.translatesAutoresizingMaskIntoConstraints = false
            desc.layer.cornerRadius = 10
            
            let picker = UIDatePicker()
            picker.datePickerMode = .countDownTimer
            

            let confirmButton = UIButton(type: .custom)
            confirmButton.setTitle("Set Timer", for: .normal)
            confirmButton.backgroundColor = UIColor.blue
            confirmButton.titleLabel!.font = UIFont.systemFont(ofSize: UIFont.buttonFontSize, weight: .bold)
            confirmButton.setTitleColor(.white, for: .normal)
            confirmButton.setTitleColor(.yellow, for: .highlighted)
            confirmButton.layer.cornerRadius = 5
            confirmButton.addTarget(self, action: #selector(timerButtonTapped(_:)), for: .touchUpInside)

            let cancelButton = UIButton(type: .custom)
            cancelButton.setTitle("Cancel", for: .normal)
            cancelButton.setTitleColor(.blue, for: .normal)
            cancelButton.setTitleColor(.red, for: .highlighted)
            cancelButton.addTarget(self, action: #selector(self.timerButtonTapped(_:)), for: .touchUpInside)
            
            let hStackView = UIStackView.init(arrangedSubviews: [confirmButton, cancelButton])
            hStackView.axis = .horizontal
            hStackView.alignment = .fill
            hStackView.distribution = .fillEqually
            hStackView.autoresizesSubviews = true
            hStackView.translatesAutoresizingMaskIntoConstraints = false
            
            let vStackView = UIStackView.init(arrangedSubviews: [title, desc, picker, hStackView])
            vStackView.axis = .vertical
            vStackView.alignment = .fill
            vStackView.distribution = .fill
            vStackView.autoresizesSubviews = true
            vStackView.translatesAutoresizingMaskIntoConstraints = false
            vStackView.spacing = 15
            
            NSLayoutConstraint(item: desc, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 32).isActive = true
            
            NSLayoutConstraint(item: desc, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 32).isActive = true
            
            NSLayoutConstraint(item: picker, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 32).isActive = true
            
            NSLayoutConstraint(item: picker, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 32).isActive = true
            
            
            // Attributes of the popup
            var attributes = EKAttributes.bottomFloat
            attributes.entryBackground = .color(color: .white)
            attributes.screenBackground = .color(color: UIColor(white: 0.5, alpha: 0.5))
            attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
            attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
            attributes.statusBar = .dark
            attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
            attributes.displayDuration = .infinity
            attributes.screenInteraction = .absorbTouches
            attributes.entryInteraction = .absorbTouches
            attributes.position = .bottom
            attributes.hapticFeedbackType = .success
            attributes.roundCorners = .all(radius: 10)
            
            attributes.lifecycleEvents.willDisappear = {
                // If cancel button was clicked, return
                if cancelButton.isHighlighted {
                    return
                }
                // Otherwise, set up a timer
                let duration = picker.countDownDuration
                
                let instruction = String((sender.view as! UITextView).text.prefix(10))
                self.setupTimer(instruction,duration)
                
            }

            SwiftEntryKit.display(entry: vStackView, using: attributes)

        }
    }
    
    @objc func timerButtonTapped(_ sender: UIButton) {
        /* Dismiss the alert and let the alert
         lifecycle callback handle the logic */
        SwiftEntryKit.dismiss(.displayed)
    }
    
    func setupTimer(_ instruction: String, _ duration: TimeInterval) {
        let timeInterval = UNTimeIntervalNotificationTrigger(timeInterval: duration,
                                                             repeats: false)
        
        // Create a notification content object
        let notificationContent = UNMutableNotificationContent()
        // Create its details
        notificationContent.title = "Time is up!"
        notificationContent.subtitle = "Your timer on instruction (\(instruction)...) has timed up"
        notificationContent.body = "You might wanna check on your cooking now!"
        let request = UNNotificationRequest(identifier: self.TIMER,
                                            content: notificationContent, trigger: timeInterval)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
