//
//  HomeTableViewController.swift
//  Diced Onions
//
//  Created by Tran Kim Thach on 3/5/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit
import FirebaseMLVision
import FirebaseFirestore
import FirebaseAuth
import SwiftEntryKit

class HomeTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UISearchResultsUpdating {
    

    
    lazy var db = Firestore.firestore()
    var recipeRef: CollectionReference? = nil
    var tagRef: CollectionReference? = nil
    var authController: Auth?
    var listener: QuerySnapshot? = nil
    
    let searchController = UISearchController(searchResultsController: nil)
    // The key:value is DocumentRef:Index in allRecipes
    var recipeDic: [String:Int] = [:]
    var allRecipes: [Recipe] = []
    var filteredRecipes: [Recipe] = []
    var tagList = [String:Bool]()
    // This holds the id of matched recipe using the current searched tags
    var idMatchedWithTags = [String:Any]()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        
        setUpDatabase()
        // Configure searchController
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Recipes"
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
        
        filteredRecipes = allRecipes
    }
    
    func setUpDatabase() {
        // Sign in
        authController = Auth.auth()
        authController?.signInAnonymously() { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
        }
        
        recipeRef = db.collection("recipes")
        tagRef = db.collection("tags")
        
        tagRef?.document("default").getDocument { (document, error) in
            if let document = document, document.exists {
                self.tagList = document.data()?["taglist"] as! [String:Bool]
                print("Fetching all tags succeeded \n \(self.tagList)")
            } else {
                print("Fetching all tags failed")
            }
        }
        
        recipeRef?.addSnapshotListener { querySnapshot, error in
            guard (querySnapshot?.documents) != nil else {
                print("Error fetching recipes: \(error!)")
                return
            }
            self.parseSnapshot(snapshot: querySnapshot!)
        }
        
    }
    
    func parseSnapshot(snapshot: QuerySnapshot) {
        snapshot.documentChanges.forEach { change in
            let documentRef = change.document.documentID
            
            let name = change.document.data()["name"] as! String
            let instructions = change.document.data()["instructions"] as! [String]
            let ingredients = change.document.data()["ingredients"] as! [String]
            let comments = change.document.data()["comments"] as! [String]
            let tags = change.document.data()["tags"] as! [String]
            
            // Add new recipe to the list
            if change.type == .added {

                let newRecipe = Recipe(id: documentRef, name: name, ingredients: ingredients, instructions: instructions, comments: comments, tags: tags)
                self.allRecipes.append(newRecipe)
                
                // Add the recognizable tags (which is every one)
                for tag in tags {
                    if tagList[tag] == true {
                        tagRef?.document("default").collection(tag).document("default").setData([documentRef:true], merge: true)
                    }
                }

            }
            
            // Find the modified recipe in the list
            // By querying the recipe dictionary
            if change.type == .modified {
                let index = recipeDic[documentRef]
                let oldTags = self.allRecipes[index!].tags
                
                print("old tags \(oldTags)")
                print("new tags \(tags)")
                /* Edit tags by removing all references to the old ones
                 then add all the new ones */
                for tag in oldTags {
                    tagRef?.document("default").collection(tag).document("default").updateData([documentRef:FieldValue.delete()])
                }
                
                for tag in tags {
                    if tagList[tag] == true {
                        tagRef?.document("default").collection(tag).document("default").setData([documentRef:true], merge: true)
                    }
                }
                
                self.allRecipes[index!].name = name
                self.allRecipes[index!].ingredients = ingredients
                self.allRecipes[index!].instructions = instructions
                self.allRecipes[index!].comments = comments
                self.allRecipes[index!].tags = tags
                
            }
            
            if change.type == .removed {
                let index = recipeDic[documentRef]
                let oldTags = self.allRecipes[index!].tags
                
                // Remove tags
                for tag in oldTags {
                    print("current tag to be deleted \(tag)")
                    tagRef?.document("default").collection(tag).document("default").updateData([documentRef:FieldValue.delete()])
                }

                self.recipeDic.removeValue(forKey: documentRef)
                self.allRecipes.remove(at: index!)
                

            }
            
            // Sort recipes alphabetically, update the dict
            allRecipes.sort(by: { $0.name! < $1.name! })
            for i in 0..<allRecipes.count {
                self.recipeDic[allRecipes[i].id!] = i
            }
            tableView.reloadData()
            
        }
    }
    
    
    // Take a picture, send it to the API to read
    @IBAction func takePhoto(_ sender: Any) {
        let controller = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            controller.sourceType = .camera
        }
        else {
            controller.sourceType = .photoLibrary
        }
        controller.allowsEditing = false
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as?
            UIImage {
            scan(pickedImage)
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func scan(_ image: UIImage) {
        let vision = Vision.vision()
        
//        let options = VisionCloudDocumentTextRecognizerOptions()
//        options.languageHints = ["en", "hi"]
//        let textRecognizer = vision.cloudTextRecognizer()

        let textRecognizer = vision.onDeviceTextRecognizer()
        
        let visionImage = VisionImage(image: image)
        
        textRecognizer.process(visionImage) { result, error in
            guard error == nil, let result = result else {
                return
            }
            self.process(result: result)
        }
    }
    
    func process(result: VisionText) {
        var ingredients: [String] = []
        var instructions: [String] = []
        
        var linesCount = 0
        var newlinesCount = 0
        var startIngr = 0
        var endIngr = 0
        var startMethod = 0
        var blocksNo = 0
        for block in result.blocks {
            blocksNo += 1
            for line in block.lines{
                let lineTxt = line.text
                linesCount += 1
                
                // Checks if the line contains heading ingredients
                if (lineTxt.lowercased().contains("ingre")){
                    startIngr = linesCount + 1
                }
                
                // Checks if the line contains relevant headings
                if (lineTxt.lowercased().contains("step") || lineTxt.lowercased().contains("method") || lineTxt.lowercased().contains("procedure") || lineTxt.lowercased().contains("direction") || lineTxt.lowercased().contains("preparation") || lineTxt.lowercased().contains("instruction")) {
                    endIngr = linesCount
                    startMethod = linesCount
                    newlinesCount = 0
                }
            }
        }
        
        for block in result.blocks {
            for line in block.lines{
                newlinesCount += 1
                if(newlinesCount >= startIngr && newlinesCount < endIngr) {
                    ingredients.append(line.text)
                    
                }
                
                if (newlinesCount > startMethod && newlinesCount < linesCount) {
                    instructions.append(line.text)
                    
                }
                
            }
        }
        
        // After processing pass these to the recipeVC to save
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "recipeViewController") as? RecipeViewController
        let newRecipe = Recipe(id: nil, name: nil, ingredients: ingredients, instructions: instructions, comments: [], tags: [])
        vc?.recipe = newRecipe
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func manuallyAdd(_ sender: Any) {
        let newRecipe = Recipe(id: nil, name: nil, ingredients: [], instructions: [], comments: [], tags: [])
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "recipeViewController") as? RecipeViewController
        vc?.recipe = newRecipe
        self.navigationController?.pushViewController(vc!, animated: true)

        
    }
    
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // If filtering, return the number of items in filterdTask
        if isFiltering() {
            return filteredRecipes.count
        }
        
        // Otherwise, return count of all tasks
        return allRecipes.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipeCell", for: indexPath)
        let recipe: Recipe
        if isFiltering() {
            recipe = filteredRecipes[indexPath.row]
        }
        else {
            idMatchedWithTags.removeAll()
            
            recipe = allRecipes[indexPath.row]
        }
        cell.textLabel?.text = recipe.name

        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let documentRef = allRecipes[indexPath.row].id!
            recipeRef?.document(documentRef).delete() { err in
                if let err = err {
                    print("Error removing recipe: \(err)")
                } else {
                    self.toastAfterDelete(message: "Recipe successfully removed!")
                    print("Recipe successfully removed!")
                }
            }
        }
        
    }
    
    // - MARK: Search functionality
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        // Returns true if the searchController is active & searchBar not empty
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        // All tasks with titles matching the search text is returned to filteredTask
        filteredRecipes = allRecipes.filter({( recipe : Recipe) -> Bool in
            let nameMatched = recipe.name?.lowercased().contains(searchText.lowercased())
            
            
            return nameMatched!
        })
        searchBasedOnTags(searchText)

    }
    

    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func searchBasedOnTags(_ searchText: String) {
        
        let tags = searchText.components(separatedBy: " ")
        
        var finished = 0
        
        print("\(tags)")
        
        for i in 0..<tags.count {
            let tag = tags[i].lowercased()
            if let _ = tagList[tag] {
                let matchedThis = tagRef?.document("default").collection(tag).document("default")
                matchedThis?.getDocument { (document, error) in
                    if let document = document, document.exists {
                        // An dictionary of IDs of matched recipe for this tag
                        let data = document.data()
                        
                        if data != nil && !data!.isEmpty {
                            let matchedIds = Array(data!.keys)
                            for j in 0..<matchedIds.count {
                                // If the recipe exists & is not already in filteredRecipes, add it to the array
                                let recipeIndex = self.recipeDic[matchedIds[j]]
                                if recipeIndex != nil {
                                    let recipe = self.allRecipes[recipeIndex!]
                                    if !self.isFiltered(recipe: recipe) {
                                        self.filteredRecipes.append(recipe)
                                    }
                                }
                            }
                        }
                        finished += 1
                        if (finished == tags.count) {
                            self.reloadAfterSearchByTag()
                        }
                    } else {
                        print("Document does not exist")
                    }
                }
            }
            else {
                finished += 1
                if (finished == tags.count) {
                    reloadAfterSearchByTag()
                }
            }
            
        }
        
    }
    
    func isFiltered(recipe: Recipe) -> Bool {
        for i in 0..<filteredRecipes.count {
            if filteredRecipes[i].id == recipe.id {
                return true
            }
        }
        
        return false
    }
    
    func reloadAfterSearchByTag() {
        self.tableView.reloadData()
        print("Reloaded")
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailsSegue" {
            if let row = tableView.indexPathForSelectedRow?.row {
                let recipe: Recipe
                if isFiltering() {
                    recipe = filteredRecipes[row]
                }
                else {
                    recipe = allRecipes[row]
                }
                let destination = segue.destination as! RecipeViewController
                destination.recipe = recipe
                
            }
        }
        
        
    }
    
}

// Function to dismiss keyboard when the view gets tapped
// Source: https://stackoverflow.com/questions/24126678/close-ios-keyboard-by-touching-anywhere-using-swift
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func toastAfterDelete(message: String) {
        
        var attributes = EKAttributes.bottomToast
        attributes.entryBackground = .color(color: .cyan)
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
        attributes.statusBar = .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.displayDuration = 1
        
        let title = EKProperty.LabelContent(text: "", style: .init(font: UIFont.systemFont(ofSize: 0.0), color: .white))
        let description = EKProperty.LabelContent(text: message, style:
            .init(font: UIFont.boldSystemFont(ofSize: 23.0), color: .black))
        let simpleMessage = EKSimpleMessage(title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
}
