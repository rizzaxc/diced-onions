# Diced Onions

Welcome to Diced Onions, your very own recipe app. The app is entirely made to assist you in building your own recipe library and facilitate your cooking.

## Features

**Home Tab**

- OCR import: click on the camera and take a picture of a recipe. The app will capture the content and transfer it to internal data.
- Manual import: click on the note icon to get a blank template to input.
- The tag system makes categorizing your recipes very easy. You can also filter the recipes (while at the Home tab) using the tags, alongside the traditional filtering by names. Be sure to hit *confirm* after selecting your tags or it won't save.
- Edit each field by long press on it. The app disables editing by default so you don't pop up the keyboard every time you want to go through the entries.
- The floating button will send an instance of the current recipe to What's Cooking tab.
- Swipe from right to left to delete the row of the table.

**What's Cooking Tab**

- This tab helps you prepare & cook your recipe.
- Clicking on each item will give you a strikethrough over it, useful when you're buying/ preping ingredients or follow through with the instructions.
- Long press on the instructions to attach a timer to it. The app will send you a local notification when time is up.

## Known issues

Currently the app has the following problems:

- When there are too many entries on the scroll view, the app won't behave as intended - it won't scroll infinitely in a sense.
- The OCR functionality is minimal; expect many bugs/ weird behaviors when presented with tricky recipe forms. It does work on the format I tested.
- The 3rd tab is for account and miscellaneous. Yet to be implemented.

## Wishlish

- Redesign the UI
- Improve OCR

## Libraries

The app uses the following libraries

- SwiftEntryKits: for the alert/ alert-like messages
- Firestore: for the database, anonymous authentication and more
- MLVision: for the OCR functionality
- [FlatIcon](https://flaticon.com): for the various icons used

## License

The app is available under the MIT license.