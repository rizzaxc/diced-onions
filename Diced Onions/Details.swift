//
//  Details.swift
//  Diced Onions
//
//  Created by Tran Kim Thach on 4/5/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit

class Details: UIView {

    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var vStackView: UIStackView!
    static let COLOR_AZUL = UIColor(red: 0.5843, green: 0.6863, blue: 0.6588, alpha: 1.0) /* #95afa8 */
    static let COLOR_MALBEC = UIColor(red: 0.2078, green: 0.1216, blue: 0.2235, alpha: 1.0) /* #351f39 */


    var preferredBackgroundColorIsAzul: Bool = true
    
    
    /*
     // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
}
