//
//  TagTableViewController.swift
//  Diced Onions
//
//  Created by Tran Kim Thach on 25/5/19.
//  Copyright © 2019 Thach Tran. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class TagTableViewController: UITableViewController, UINavigationControllerDelegate {
    
    lazy var db = Firestore.firestore()
    var tagRef: CollectionReference? = nil
    var authController: Auth?
    var listener: QuerySnapshot? = nil
    
    var selected = [String: Bool]()
    

    var cuisine: [String] = []
    var dish_type: [String] = []
    var main_ingredient: [String] = []
    var tagCollections = [[String]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        setupTags()
        print("SELECTED: \(selected)")
    
    }
    
    func setupTags() {
        
        cuisine = ["Asian", "Indian", "Western", "Chinese", "Japanese",
                   "Korean", "Thai", "Vietnamese", "American", "French", "Italian", "Mediterranean"].sorted()
        dish_type = ["Dessert", "Entree", "Main", "Fastfood"].sorted()
        main_ingredient = ["Beef", "Chicken", "Egg", "Fruit", "Lamb", "Vegetables"].sorted()
        
        // The first item is the favorite tag
        tagCollections.append(["Favorites"])
        tagCollections.append(cuisine)
        tagCollections.append(main_ingredient)
        tagCollections.append(dish_type)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // 4 sections: Favorites, Cuisines, Main Ingredient, Dish Types
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tagCollections[section].count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return ""
        case 1:
            return "Cuisines"
        case 2:
            return "Main Ingredients"
        default:
            return "Dish Types"
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tagCell", for: indexPath) as! TagTableViewCell
        let tag = tagCollections[indexPath.section][indexPath.row]
        
        
        // If the tag matches the ones from Details, tick it
        cell.textLabel?.text = tag
        if selected[tag.lowercased()] != nil {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tag = tagCollections[indexPath.section][indexPath.row]
        selected[tag.lowercased()] = true
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let tag = tagCollections[indexPath.section][indexPath.row]
        selected.removeValue(forKey: tag.lowercased())
    }

}
